"""Functions for work with date"""
import datetime


def get_today():
    """Returns today's date str in DD.MM"""
    today_date = datetime.date.today().strftime("%d.%m")
    return today_date


def get_tommorow():
    """Returns tommorow's date str in DD.MM"""
    tommorow_date = datetime.date.today()
    tommorow_date = "%d.%d" % (tommorow_date.day + 1, tommorow_date.month)
    return tommorow_date


def get_plus_hour():
    """Return plus one hour from current time"""
    plus_hour = "%d:%d" % (
        datetime.datetime.now().hour + 1, datetime.datetime.now().minute
        )
    return plus_hour


def get_plus_three_hours():
    """Return plus one hour from current time"""
    plus_three_hour = "%d:%d" % (
        datetime.datetime.now().hour + 3, datetime.datetime.now().minute
        )
    return plus_three_hour
