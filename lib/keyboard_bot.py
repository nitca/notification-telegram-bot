"""Keyboard for notification telegram bot"""


import config
from telebot.types import KeyboardButton, ReplyKeyboardMarkup,\
    InlineKeyboardButton, InlineKeyboardMarkup


# Main menu keyboard and buttons
mainmenu_kb = ReplyKeyboardMarkup(resize_keyboard=True)
button_control_not = KeyboardButton(config.Button.control_notify)
button_create_not = KeyboardButton(config.Button.create_notify)

mainmenu_kb.add(button_create_not)
mainmenu_kb.add(button_control_not)

# Back menu keyboard and buttons
back_kb = ReplyKeyboardMarkup(resize_keyboard=True)
button_back = KeyboardButton(config.Button.back)
back_kb.add(button_back)


# Inline keyboard create notify date
kb_create_date = InlineKeyboardMarkup()
btn_create_today = InlineKeyboardButton(
    config.InlButton.text_today, callback_data=config.InlButton.data_today
    )
btn_create_tommorow = InlineKeyboardButton(
    config.InlButton.text_tommorow,
    callback_data=config.InlButton.data_tommorow
    )
btn_create_custom = InlineKeyboardButton(
    config.InlButton.text_custom, callback_data=config.InlButton.data_custom
    )
kb_create_date.add(btn_create_today, btn_create_tommorow)
kb_create_date.add(btn_create_custom)


# Inline keyboard create notify time
kb_create_time = InlineKeyboardMarkup()
btn_create_hour = InlineKeyboardButton(
    config.InlButton.text_one_hour,
    callback_data=config.InlButton.data_one_hour
    )
btn_create_three_hours = InlineKeyboardButton(
    config.InlButton.text_three_hours,
    callback_data=config.InlButton.data_three_hours
    )
btn_create_custon_hour = InlineKeyboardButton(
    config.InlButton.text_custom_hour,
    callback_data=config.InlButton.data_custom_hour
    )

kb_create_time.add(btn_create_hour, btn_create_three_hours)
kb_create_time.add(btn_create_custon_hour)
