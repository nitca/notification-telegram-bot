"""Function for check user's key in callback keyboard of telegram bot"""
import re


def check_call_btn(call_data, mode):
    """Check for user's buton type in inline keyboards"""
    if mode == 'create_date':
        if re.search(r'^create_(today|tommorow|custom)$', call_data):
            return True
    if mode == 'create_time':
        if re.search(r'create_(hour|thours|custom_hour)', call_data):
            return True
    return False
