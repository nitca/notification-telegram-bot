"""Telegram notify bot's main file"""
import telebot
import config
import lib.telebot_user_state as telebot_user_state
import lib.check_callback_keys_functions as check_callback_keys_functions
import lib.keyboard_bot as keyboard_bot
import lib.date_functions as date_functions


bot = telebot.TeleBot(config.TOKEN)
telebot_user_state.init_states()


def back_to_main_menu(msg):
    """Sends user to main menu"""
    telebot_user_state.drop_data(msg.chat.id)
    telebot_user_state.update_state(msg.chat.id, config.States.main_menu)
    bot.send_message(
        msg.chat.id, 'Главное меню', reply_markup=keyboard_bot.mainmenu_kb
        )


@bot.message_handler(commands=['start'])
def start_func(msg):
    """Start function"""
    telebot_user_state.add_user_in_data(msg.chat.id)
    back_to_main_menu(msg)


@bot.message_handler(
    func=lambda msg:
        telebot_user_state.is_current_state(
            msg.chat.id, [config.States.main_menu]
            )
        )
def main_menu(msg):
    """Main menu buttons ligic"""
    if msg.text == config.Button.create_notify:
        telebot_user_state.update_state(msg.chat.id, config.States.create_menu)
        bot.send_message(
            msg.chat.id,
            'Меню создания задачи',
            reply_markup=keyboard_bot.back_kb
            )
        bot.send_message(
            msg.chat.id,
            'Выберите день или укажите вручную:\n',
            reply_markup=keyboard_bot.kb_create_date
            )
    if msg.text == config.Button.control_notify:
        telebot_user_state.update_state(
            msg.chat.id, config.States.control_menu
            )
        bot.send_message(
            msg.chat.id,
            'Меню управления задачами',
            reply_markup=keyboard_bot.back_kb
            )


# All create notification menu
@bot.message_handler(
    func=lambda msg:
        telebot_user_state.is_current_state(
            msg.chat.id, [config.States.create_menu]
            )
        )
def create_notify_menu(msg):
    """Menu for create user's notify"""
    if msg.text == config.Button.back:
        telebot_user_state.update_state(msg.chat.id, config.States.main_menu)
        back_to_main_menu(msg)
        return


@bot.callback_query_handler(
    func=lambda call: check_callback_keys_functions.check_call_btn(
        call.data, 'create_date'
        )
    )
def inl_create_date(call):
    """Inline keyboard's logic for create date's notification"""
    time_chose_text = 'Вы можете выбрать время спустя час, три часа'\
        ' от текущего времени\nИли ввести время вручную:'
    if call.data == config.InlButton.data_today:
        bot.edit_message_text(
            time_chose_text, call.message.chat.id,
            call.message.message_id,
            reply_markup=keyboard_bot.kb_create_time
            )
        telebot_user_state.add_data(
            call.message.chat.id, 'date', date_functions.get_today()
            )
    if call.data == config.InlButton.data_tommorow:
        bot.edit_message_text(
            time_chose_text, call.message.chat.id,
            call.message.message_id,
            reply_markup=keyboard_bot.kb_create_time
            )
        telebot_user_state.add_data(
            call.message.chat.id, 'date', date_functions.get_tommorow()
            )
    if call.data == config.InlButton.data_custom:
        bot.edit_message_text(
            'Введите дату:', call.message.chat.id,
            call.message.message_id
            )
        telebot_user_state.update_state(
            call.message.chat.id, config.States.create_menu_date
            )


# Custom user's input date
@bot.message_handler(
    func=lambda msg:
        telebot_user_state.is_current_state(
            msg.chat.id, [config.States.create_menu_date]
            )
        )
def custom_date(msg):
    """Takes custom date from user if forman DD.MM"""
    if msg.text == config.Button.back:
        back_to_main_menu(msg)
        return

    time_chose_text = 'Вы можете выбрать время спустя час, три часа'\
        ' от текущего времени\nИли ввести время вручную:'

    bot.send_message(
        msg.chat.id, time_chose_text, reply_markup=keyboard_bot.kb_create_time
        )
    telebot_user_state.add_data(
        msg.chat.id, 'date', msg.text
        )
    telebot_user_state.update_state(
        msg.chat.id, config.States.create_menu
        )


@bot.callback_query_handler(
    func=lambda call: check_callback_keys_functions.check_call_btn(
        call.data, 'create_time'
        )
    )
def inl_create_time(call):
    """Inline's keyboard logic for get user notification's time"""
    if call.data == config.InlButton.data_one_hour:
        bot.edit_message_text(
            text='Введите заголовок задачи:',
            chat_id=call.message.chat.id,
            message_id=call.message.message_id
            )
        telebot_user_state.add_data(
            call.message.chat.id, 'time', date_functions.get_plus_hour()
            )
        telebot_user_state.update_state(
            call.message.chat.id, config.States.create_menu_title
            )
    if call.data == config.InlButton.data_three_hours:
        bot.edit_message_text(
            text='Введите заголовок задачи:',
            chat_id=call.message.chat.id,
            message_id=call.message.message_id
            )
        telebot_user_state.add_data(
            call.message.chat.id, 'time', date_functions.get_plus_three_hours()
            )
        telebot_user_state.update_state(
            call.message.chat.id, config.States.create_menu_title
            )
    if call.data == config.InlButton.data_custom_hour:
        bot.edit_message_text(
            'Введите время:',
            call.message.chat.id,
            message_id=call.message.message_id
            )
        telebot_user_state.update_state(
            call.message.chat.id, config.States.create_menu_time
            )


# Custom user's task time
@bot.message_handler(
    func=lambda msg:
        telebot_user_state.is_current_state(
            msg.chat.id, [config.States.create_menu_time]
            )
        )
def custom_time(msg):
    """Takes custom date from user if forman DD.MM"""
    if msg.text == config.Button.back:
        back_to_main_menu(msg)
        return

    bot.send_message(msg.chat.id, 'Введите заголовок задачи:')
    telebot_user_state.add_data(
        msg.chat.id, 'time', msg.text
        )
    telebot_user_state.update_state(
        msg.chat.id, config.States.create_menu_title
        )


@bot.message_handler(
    func=lambda msg:
        telebot_user_state.is_current_state(
            msg.chat.id, [config.States.create_menu_title]
            )
        )
def get_title_task(msg):
    """Gets user's task title"""
    if msg.text == config.Button.back:
        back_to_main_menu(msg)
        return

    bot.send_message(msg.chat.id, 'Введите описание задачи:')
    telebot_user_state.add_data(
        msg.chat.id, 'title', msg.text
        )
    telebot_user_state.update_state(
        msg.chat.id, config.States.create_menu_discr
        )


@bot.message_handler(
    func=lambda msg:
        telebot_user_state.is_current_state(
            msg.chat.id, [config.States.create_menu_discr]
            )
        )
def get_discription_task(msg):
    """Final step in creation task. Gets task's discription from user"""
    if msg.text == config.Button.back:
        back_to_main_menu(msg)
        return
    telebot_user_state.add_data(
        msg.chat.id, 'discr', msg.text
        )
    bot.send_message(msg.chat.id, 'Задача успешно создана!')
    back_to_main_menu(msg)


@bot.message_handler(
    func=lambda msg:
        telebot_user_state.is_current_state(
            msg.chat.id, [config.States.control_menu]
            )
        )
def control_notify_menu(msg):
    """Menu for create user's notify"""
    if msg.text == config.Button.back:
        back_to_main_menu(msg)
        return


if __name__ == '__main__':
    bot.infinity_polling(timeout=120)
