"""All notify manager bot data"""

TOKEN = '1274460672:AAGqhOTMrbWmBWNP-qyViRYKR0m5zoXCwL8'


class Button:
    """All text buttons for keyboard"""
    create_notify = 'Создать задачу[📅]'
    control_notify = 'Управление задачами[🎛]'
    list_notify = 'Список задач[🗒]'
    back = 'Назад[◀️]'


class InlButton:
    """All inline buttons text and call_data"""
    text_today = 'Сегодня'
    text_tommorow = 'Завтра'
    text_custom = 'Вручную'

    text_one_hour = 'Один час'
    text_three_hours = 'Три часа'
    text_custom_hour = 'Вручную'

    data_today = 'create_today'
    data_tommorow = 'create_tommorow'
    data_custom = 'create_custom'

    data_one_hour = 'create_hour'
    data_three_hours = 'create_thours'
    data_custom_hour = 'create_custom_hour'


class States:
    """Users states for telegram_user_state library"""
    main_menu = 'main_menu'
    create_menu = 'create_menu'
    create_menu_time = 'create_menu_time'
    create_menu_date = 'create_menu_date'
    create_menu_title = 'create_menu_title'
    create_menu_discr = 'create_menu_discr'
    control_menu = 'control_menu'
